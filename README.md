# README #

This is a very basic example of one way to do authentication in an AngularJS SPA. It uses the default Angular project created by the Yeoman Angular generator with 3 new controllers and views:

* Login - Contains a form to login to the app
* Logout - Logs the user out and redirects to the home view
* SuperSecret - A view that only logged in users should be able to see

There is also a service authenticationService which handles the API calls to log the user in/out with the API server and the maintaining of the current users state in session.

## Setting up ##
There are actually 2 separate apps in the repo:

* ~/src/client - contains the AngularJS App
* ~/src/server - contains the LoopBack API Server

To setup locally, you will need Node and NPM and Grunt installed, then do:

```
$ git clone git@bitbucket.org:jonratcliffe/angularjs-authentication-example.git

$ cd angularjs-authentication-example/src/server
$ npm install
$ node . &

$ cd ../client
$ npm install
$ bower install
$ grunt serve
```

## Based on ##
This was pulled together from 3 excellent tutorials:

* [https://github.com/strongloop/loopback-example-access-control](https://github.com/strongloop/loopback-example-access-control)
* [http://www.sitepoint.com/implementing-authentication-angular-applications/](http://www.sitepoint.com/implementing-authentication-angular-applications/)
* [http://coder1.com/articles/angularjs-managing-active-nav-elements](http://coder1.com/articles/angularjs-managing-active-nav-elements)