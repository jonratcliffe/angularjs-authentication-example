"use strict";

/**
 * @ngdoc service
 * @name clientApp.authenticationService
 * @description
 * # authenticationService
 * Service in the clientApp.
 */
angular.module("clientApp")
  .factory("authenticationService", function ($http, $q, $window) {
    var userInfo;

    function init(){
      if ($window.sessionStorage.userInfo) {
        userInfo = JSON.parse($window.sessionStorage.userInfo);
      }
    };

    function login(email, password) {
      var deferred = $q.defer();

      $http.post("http://localhost:3000/api/users/login", {
        email: email,
        password: password
      }).then(function (result) {
        userInfo = {
          accessToken: result.data.id,
          id: result.data.userId,
          email: email
        };
        $window.sessionStorage.userInfo = JSON.stringify(userInfo);
        deferred.resolve(userInfo);
      }, function (error) {
        deferred.reject(error);
      });

      return deferred.promise;
    }

    function logout(){
      var deferred = $q.defer();

      if(userInfo === null){
        deferred.error("User not logged in.");
        return deferred;
      }

      $http.post("http://localhost:3000/api/users/logout?access_token=" + userInfo.accessToken).then(function (result) {
        userInfo = null;
        $window.sessionStorage.userInfo = userInfo;
        deferred.resolve(result);
      },
      function (error) {
        deferred.reject(error);
      });

      return deferred.promise;

    }
    function getUserInfo(){
      return userInfo;
    };

    init();

    return{
      login:login,
      logout: logout,
      getUserInfo: getUserInfo
    };

  });
