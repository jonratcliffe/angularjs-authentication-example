'use strict';

/**
 * @ngdoc function
 * @name clientApp.controller:LoginCtrl
 * @description
 * # LoginCtrl
 * Controller of the clientApp
 */
angular.module("clientApp")
  .controller("LoginCtrl", function ($scope, $location, authenticationService) {
    $scope.email = "";
    $scope.password = "";

    $scope.processLogin = function() {
      console.log("Processing Login");
      authenticationService.login($scope.email, $scope.password).then(
        function(userInfo) {
          console.log(userInfo);
          $location.path("/supersecret");
        },
        function(err){
          console.log(err);
        }
      );
    };
  });
