'use strict';

/**
 * @ngdoc function
 * @name clientApp.controller:LogoutCtrl
 * @description
 * # LogoutCtrl
 * Controller of the clientApp
 */
angular.module('clientApp')
  .controller('LogoutCtrl', function ($scope, $location, authenticationService) {
    authenticationService.logout().then(
      function(){
        $location.path("/");
      },
      function(err){
        console.log(err);
      }
    );

  });
