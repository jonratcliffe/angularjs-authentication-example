'use strict';

/**
 * @ngdoc function
 * @name clientApp.controller:PrimarynavCtrl
 * @description
 * # PrimarynavCtrl
 * Controller of the clientApp
 */
angular.module('clientApp')
  .controller('PrimaryNavCtrl', ['$scope', '$location', 'authenticationService', function($scope, $location, authenticationService) {
    $scope.$location = $location;

    $scope.userIsLoggedIn = function(){
      return authenticationService.getUserInfo() !== null;
    };
  }]);
